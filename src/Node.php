<?php


namespace Graph;


class Node
{
    public $name;
    public $status;

    public function __construct($name, $status) {
        $this->name = $name;
        $this->status = $status;
    }
}