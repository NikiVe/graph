<?php

namespace Graph;

class Graph
{
    protected bool $saveVisit = false;

    protected $table = [];
    protected $nodesName = [];
    protected int $size = 0;

    protected NodeList $vnl;

    public function __construct(array $table, $nodesName = []) {
        if (count($table) !== count($nodesName)) {
            $count = count($table);
            for ($i = 0; $i < $count; $i++) {
                $nodesName[] = $i;
            }
        }

        $this->table = $table;
        $this->nodesName = $nodesName;
        $this->size = count($table);

        $this->resetVnl();
    }

    protected function resetVnl() {
        $this->vnl = new NodeList($this->nodesName);
    }

    public function setSaveVisit(bool $saveVisit) {
        $this->saveVisit = $saveVisit;
        return $this;
    }

    public function getTable() {
        return $this->table;
    }

    public function getNodesName() {
        return $this->nodesName;
    }

    public function getNeighbours($nodeName) {
        $p = array_search($nodeName, $this->nodesName, true);
        if ($p === false) {
            throw new \RuntimeException('Такого элемента не существует');
        }
        $result = [];
        foreach ($this->table[$p] as $num => $weight) {
            if ($weight) {
                $result[$this->nodesName[$num]] = $this->nodesName[$num];
            }
        }
        return $result;
    }

    public function dfs($start, $end = '') {
        return $this->fs(new Queue(Queue::FILO), $start, $end);
    }

    public function bfs($start, $end) {
        return $this->fs(new Queue(Queue::FIFO), $start, $end);
    }

    protected function fs(Queue $queue, $start, $end) {
        $result = [];

        $this->vnl->setVisited($start);
        $queue->push($start);

        while ($queue->getLength() > 0) {
            $v = $queue->pop();
            $result[] = $v;
            $neighbourList = $this->getNeighbours($v);
            foreach ($neighbourList as $item) {
                if (!$this->vnl->isVisited($item)) {
                    $queue->push($item);
                    $this->vnl->setVisited($item);
                    if ($item === $end) {
                        $result[] = $end;
                        $queue->reset();
                        break;
                    }
                }
            }
        }

        if (!$this->saveVisit) {
            $this->resetVnl();
        }

        return array_reverse($result);
    }

    public function getVNL() {
        return $this->vnl;
    }

    public function getSize() {
        return $this->size;
    }

    public static function createBySize(int $size, $nodesName = []) {
        $table = [];
        for ($i = 0; $i < $size; $i++) {
            for ($j = 0; $j < $size; $j++) {
                $table[$i][$j] = 0;
            }
        }
        return new self($table, $nodesName);
    }

    public function addEdge(Edge $edge) {
        $p1 = array_search($edge->n1, $this->nodesName);
        $p2 = array_search($edge->n2, $this->nodesName);
        if ($p1 !== false && $p2 !==false) {
            $this->table[$p1][$p2] = $edge->size;
            $this->table[$p2][$p1] = $edge->size;
        } else {
            throw new \RuntimeException();
        }
    }
}