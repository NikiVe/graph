<?php


namespace Graph;


class NodeList
{
    protected $nodeList = [];

    public function __construct($array) {
        foreach ($array as $item) {
            $this->addNode($item);
        }
    }

    public function addNode($name) {
        $this->nodeList[$name] = new Node($name, false);
    }

    public function setVisited($name, $flag = true) {
        if (isset($this->nodeList[$name])) {
            $this->nodeList[$name]->status = $flag;
        } else {
            throw new \RuntimeException();
        }
    }

    public function isVisited($name) {
        if (isset($this->nodeList[$name])) {
            return $this->nodeList[$name]->status;
        }
        throw new \RuntimeException();
    }
}