<?php


namespace Graph;


class Edge
{
    public string $n1;
    public string $n2;
    public int $size;

    public function __construct(string $n1, string $n2, int $size) {
        $this->n1 = $n1;
        $this->n2 = $n2;
        $this->size = $size;
    }
}