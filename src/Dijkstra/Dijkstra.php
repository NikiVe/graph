<?php


namespace Graph\Dijkstra;


use Graph\Edge;
use Graph\Graph;

class Dijkstra
{
    protected Graph $graph;
    protected string $start;
    protected string $end;

    protected array $nodeList = [];
    protected array $usedNode = [];

    protected function __construct(Graph $graph, $start, $end) {
        $this->graph = $graph;
        $this->start = $start;
        $this->end = $end;
    }

    protected function work() {
        $this->initWorkList();

        $this->nodeList[$this->start]->setSize(0);

        while ($next = $this->getNextNode()) {
            /** @var Edge $item */
            foreach ($next->getEdgeList() as $item) {
                if (in_array($item->n2, $this->usedNode)) {
                    continue;
                }

                $size = $next->getSize() + $item->size;

                if ($this->nodeList[$item->n2]->isInfinity() ||
                    $this->nodeList[$item->n2]->getSize() > $size
                ) {
                    $this->nodeList[$item->n2]->setSize($size, $this->getNodeByName($item->n1));
                }
            }
            $this->usedNode[] = $next->getName();
        }
        return $this->getWay();
    }

    /**
     * @return Node|null
     */
    protected function getNextNode() {
        $next = null;
        /** @var Node $item */
        foreach ($this->nodeList as $item) {
            if (in_array($item->getName(), $this->usedNode, true)) {
                continue;
            }

            if (!$item->isInfinity()) {
                if ($next === null) {
                    $next = $item;
                } else {
                    if ($next->getSize() > $item->getSize()) {
                        $next = $item;
                    }
                }
            }
        }
        return $next->getName() === $this->end ? null : $next;
    }

    protected function getNodeByName($name) {
        /** @var Node $node */
        foreach ($this->nodeList as $node) {
            if ($node->getName() === $name) {
                return $node;
            }
        }
        throw new \RuntimeException();
    }

    protected function getWay() {
        $next = $this->getNodeByName($this->end);
        $result = [];
        while ($next->getNodeIn()) {
            $result[] = $next->getNodeInEdge();
            $next = $next->getNodeIn();
        }
        return $result;
    }

    protected function initWorkList() {
        foreach ($this->graph->getNodesName() as $num => $name) {
            $this->nodeList[$name] = new Node($name, $this->graph->getTable()[$num], $this->graph->getNodesName());
        }
    }

    public static function start(Graph $graph, $start, $end) {
        $alg = new self($graph, $start, $end);
        return $alg->work();
    }
}