<?php


namespace Graph\Dijkstra;


use Graph\Edge;
use http\Exception\RuntimeException;

class Node
{
    protected string $size;
    protected string $name;
    protected ?Node $nodeIn = null;

    protected array $edgeList = [];

    public function __construct(string $name, array $edges = [], array $nodeNames = []) {
        $this->size = '*';
        $this->name = $name;

        $this->initEdgeList($edges, $nodeNames);
    }

    protected function initEdgeList(array $edges, array $nodeList) {
        if (empty($nodeList)) {
            $nodeList = array_keys($edges);
        }
        foreach ($edges as $num => $size) {
            if ((int)$size) {
                $this->edgeList[] = new Edge($this->name, $nodeList[$num], $size);
            }
        }
    }

    public function getSize() : int {
        if ($this->isInfinity()) {
            throw new \RuntimeException();
        }
        return (int)$this->size;
    }
    public function isInfinity() {
        return $this->size === '*';
    }

    public function setSize(int $size, ?Node $nodeIn = null) {
        $this->size = $size;
        if ($nodeIn) {
            $this->nodeIn = $nodeIn;
        }
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name = $name;
        return $this;
    }

    public function getNodeIn() {
        return $this->nodeIn;
    }

    public function setNodeIn(Node $nodeIn) {
        $this->nodeIn = $nodeIn;
        return $this;
    }

    public function getEdgeList() {
        return $this->edgeList;
    }

    public function getNodeInEdge() {
        /** @var Edge $edge */
        foreach ($this->edgeList as $edge) {
            if ($edge->n1 === $this->name && $this->nodeIn->getName() === $edge->n2) {
                return $edge;
            }
        }
        return null;
    }
}