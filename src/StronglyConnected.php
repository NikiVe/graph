<?php


namespace Graph;


class StronglyConnected
{
    protected Graph $graph;
    protected Graph $invGraph;

    protected function __construct(Graph $graph) {
        $this->graph = clone $graph;
        $this->graph->setSaveVisit(true);
    }

    public static function start(Graph $graph) {
        $alg = new self($graph);
        $alg->buildInversionGraph();
        $sequence = $alg->stageTwo();
        return $alg->stageTree($sequence);
    }

    /**
     * Инвертируем дуги исходного ориентированного графа.
     *
     * @throws \Exception
     */
    protected function buildInversionGraph() {
        $table = $this->graph->getTable();
        $newTable = [];
        $c = count($table);
        for ($i = 0; $i < $c; $i++) {
            for ($j = 0; $j < $c; $j++) {
                $newTable[$j][$i] = $table[$i][$j];
            }
        }

        $this->invGraph = (new Graph($newTable, $this->graph->getNodesName()))->setSaveVisit(true);
    }

    /**
     * Запускаем поиск в глубину на этом обращённом графе, запоминая, в каком порядке выходили из вершин.
     */
    public function stageTwo() {
        $result = [];
        $v = $this->invGraph->getNodesName()[array_key_first($this->invGraph->getNodesName())];
        $lost = $this->invGraph->getNodesName();
        while (count($this->invGraph->getNodesName()) !== count($result)) {
            $inner = $this->invGraph->dfs($v);

            $result = array_merge($result, $inner);

            $lost = array_diff($lost, $inner);
            if (empty($lost)) {
                break;
            }
            $v = array_pop($lost);
        }
        return $result;
    }

    public function stageTree($sequence) {
        $result = [];
        while (count($sequence)) {
            $nodes = $this->graph->dfs($sequence[array_key_last($sequence)]);
            $sequence = array_diff($sequence, $nodes);
            $result[] = new Component($nodes);
        }
        return $result;
    }

}