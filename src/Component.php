<?php


namespace Graph;


class Component
{
    public array $items = [];

    public function __construct($items) {
        $this->items = $items;
    }
}