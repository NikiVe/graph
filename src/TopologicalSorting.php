<?php


namespace Graph;


class TopologicalSorting
{
    protected array $table;
    protected int $sizeTable;

    protected array $vector = [];

    protected array $levels = [];

    protected function __construct(Graph $graph) {
        $this->table = $graph->getTable();
        $this->sizeTable = $graph->getSize();
        $this->initVector();
    }

    protected function getZeroLevel() {
        $result = [];
        $newVector = [];
        foreach ($this->vector as $num => $sum) {
            $sum === 0 ? $result[] = $num : $newVector[$num] = $sum;
        }

        if (!empty($result)) {
            $this->levels[] = $result;
            $this->vector = $newVector;
        }

        return $result;
    }

    protected function getSumLine($num) {
        $sum = 0;
        for ($i = 0; $i < count($this->table); $i++){
            $sum += $this->table[$i][$num];
        }
        return $sum;
    }

    protected function getTable() {
        return $this->table;
    }

    protected function initVector() {
        for ($i = 0; $i < $this->sizeTable; $i++) {
            $this->vector[$i] = $this->getSumLine($i);
        }
    }

    protected function work() {
        while ($zeroLevels = $this->getZeroLevel()) {
            foreach ($zeroLevels as $level) {
                for ($j = 0; $j < count($this->getTable()); $j++) {
                    if ($this->table[$level][$j]) {
                        $this->vector[$j]--;
                    }
                }
            }
        }
        return $this->levels;
    }

    public static function start(Graph $graph) {
        $alg = new self($graph);
        return $alg->work();
    }
}