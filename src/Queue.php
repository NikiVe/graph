<?php


namespace Graph;


class Queue
{
    protected $stack = [];

    public const FIFO = 0;
    public const FILO = 1;

    protected $type;
    protected $length = 0;

    public function __construct($type) {
        $this->type = $type;
    }

    public function push($value) {
        $this->stack[] = $value;
        $this->length++;
    }

    public function pop() {
        if ($this->length < 1) {
            throw new \RuntimeException();
        }
        $this->length--;
        return $this->type === self::FIFO ? array_shift($this->stack) : array_pop($this->stack) ;
    }

    public function isset($value) {
        return in_array($value, $this->stack, true);
    }

    public function getLength() {
        return $this->length;
    }

    public function reset() {
        $this->stack = [];
        $this->length = 0;
    }
}