<?php


namespace Graph;


class Kruskal
{
    protected Graph $graph;
    protected Graph $tree;
    protected array $edgeList;

    protected function __construct(Graph $graph) {
        $this->graph = $graph;
    }

    protected function initEdgeList() {
        for ($i = 0; $i < $this->graph->getSize(); $i++) {
            for ($j = $i; $j < $this->graph->getSize(); $j++) {
                if ($this->graph->getTable()[$i][$j]) {
                    $this->edgeList[] = new Edge(
                        $this->graph->getNodesName()[$i],
                        $this->graph->getNodesName()[$j],
                        $this->graph->getTable()[$i][$j]
                    );
                }
            }
        }
        uasort($this->edgeList, function ($a, $b) {
            return $a->size > $b->size;
        });
    }

    public function work() {
        $this->initEdgeList();
        $this->tree = Graph::createBySize($this->graph->getSize(), $this->graph->getNodesName());

        $result = [];
        foreach ($this->edgeList as $edge) {
            $r = $this->tree->bfs($edge->n1, $edge->n2);
            if ($r[0] !== $edge->n2) {
                $result[] = $edge;
                $this->tree->addEdge($edge);
            }
        }
        return $result;
    }

    public static function start(Graph $graph) {
        $alg = new self($graph);
        return $alg->work();
    }
}