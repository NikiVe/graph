<?php
include __DIR__ . '/vendor/autoload.php';

$table = [
    ['0', '7',  '9', ' 0',  '0', '14'],
    ['7', '0',  '10', '15', '0', '0'],
    ['9', '10', '0',  '11', '0', '2'],
    ['0', '15', '11', '0',  '6', '0'],
    ['0', '0',  '0',  '0',  '6', '9'],
    ['14','0',  '2',  '0',  '9', '0'],
];

$graph = new \Graph\Graph($table, $nodeList = [1, 2, 3, 4, 5, 6]);

$result = \Graph\Dijkstra\Dijkstra::start($graph, 1, 4);

print_r($result);